export default {
  path: "/mes",
  redirect: "",
  meta: {
    icon: "ri:bar-chart-horizontal-line",
    title: "功能开发（DEV）",
    rank: 999
  },
  children: [
    {
      path: "/mes/master/line/index",
      name: "MesMasterLine",
      component: () => import("@/views/mes/master/line/index.vue"),
      meta: {
        title: "产线管理"
      }
    },
    {
      path: "/mes/master/workshop/index",
      name: "MesMasterWorkshop",
      component: () => import("@/views/mes/master/workshop/index.vue"),
      meta: {
        title: "车间管理"
      }
    },
    {
      path: "/mes/master/workstation/index",
      name: "MesMasterWorkstation",
      component: () => import("@/views/mes/master/workstation/index.vue"),
      meta: {
        title: "工位管理"
      }
    },
    {
      path: "/mes/master/material/index",
      name: "MesMasterMaterial",
      component: () => import("@/views/mes/master/material/index.vue"),
      meta: {
        title: "物料管理"
      }
    },
    {
      path: "/mes/process/operation/index",
      name: "MesProcessOperation",
      component: () => import("@/views/mes/process/operation/index.vue"),
      meta: {
        title: "工序定义"
      }
    },
    {
      path: "/mes/process/routing/index",
      name: "MesProcessRoute",
      component: () => import("@/views/mes/process/routing/index.vue"),
      meta: {
        title: "工艺路线"
      },
      children: null
    },
    {
      path: "/mes/process/routeflow/index",
      name: "MesProcessRouteFlow",
      component: () => import("@/views/mes/process/routeflow/index.vue"),
      meta: {
        title: "路线流程图",
        showLink: false
      }
    }
  ]
} satisfies RouteConfigsTable;
