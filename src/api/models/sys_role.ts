import type { SysRoleMenu } from "./sys_role_menu";

/** 系统角色 */
export interface SysRole {
  /** 角色Id */
  id: number;
  /** 角色名称 */
  name: string;
  /** 角色编号 */
  code: string;
  /** 排序 */
  sort: number;
  /** 状态：1->启用；0->停用 */
  status: number;
  /** 备注 */
  remark?: string;
  /** 角色菜单关系集合 */
  roleMenus: SysRoleMenu[];
}
