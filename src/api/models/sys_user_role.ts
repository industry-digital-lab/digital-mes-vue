/** 用户角色对应关系 */
export interface UserRoleInfo {
  id: number;
  /** 用户 Id */
  userId: number;
  /** 角色 Id */
  roleId: number;
}
