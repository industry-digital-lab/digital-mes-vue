/** 角色菜单对应关系 */
export interface SysRoleMenu {
  id: number;
  /** 角色 Id */
  roleId: number;
  /** 菜单 Id */
  menuId: number;
}
