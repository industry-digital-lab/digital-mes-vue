/** 系统菜单 */
export interface SysMenu {
  /** 菜单Id */
  id: number;
  /** 菜单类型（0代表菜单、1代表iframe、2代表外链、3代表按钮）*/
  menuType: number;
  parentId: number;
  title?: string;
  name?: string;
  path?: string;
  component?: string;
  rank: number;
  redirect?: string;
  icon?: string;
  extraIcon?: string;
  enterTransition?: string;
  leaveTransition?: string;
  activePath?: string;
  auths?: string;
  frameSrc?: string;
  frameLoading: boolean;
  keepAlive: boolean;
  hiddenTag: boolean;
  fixedTag: boolean;
  showLink: boolean;
  showParent: boolean;
}

/** 菜单类型枚举 */
export enum MenuType {
  Menu = "菜单",
  Iframe = "iframe",
  Link = "外链",
  Button = "按钮"
}
