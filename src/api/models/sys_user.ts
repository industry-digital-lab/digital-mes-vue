import type { UserRoleInfo } from "./sys_user_role";

/** 用户信息 */
export interface UserInfo {
  /** 用户id */
  id: number;
  /** 用户名 */
  userName?: string;
  /** 昵称 */
  nickname?: string;
  /** 头像 */
  avatar?: string;
  /** 邮箱 */
  email?: string;
  /** 联系电话 */
  phone?: string;
  /** 简介 */
  description?: string;
  /** 性别 */
  gender: number;
  /** 组织Id */
  orgId?: number;
  /** 组织 */
  org?: {
    id: number;
    name: string;
  };
  /** 状态：1->启用；0->停用 */
  status: number;
  /** 用户角色对应关系集合 */
  userRoles: UserRoleInfo[];
}
