import { httpClient } from "@/api/httpWrapper";

/** 获取生产线列表 */
export const getLineList = (params?: object) => {
  return httpClient.get<any[]>("/api/mes/line/list", params);
};
