/** API 请求返回结果接口 */
export interface Result<T = any> {
  /** 状态码 */
  code: number;
  /** 返回结果 */
  success: boolean;
  /** 消息 */
  message: string;
  /** 数据 */
  data?: T;
}

/** API 请求返回分页数据结果接口 */
export interface PageResult<T = any> {
  /** 状态码 */
  code: number;
  /** 返回结果 */
  success: boolean;
  /** 消息 */
  message: string;
  /** 数据 */
  data?: {
    /** 列表数据 */
    items: T[];
    /** 总页数 */
    totalPages: number;
    /** 总条数 */
    totalCount: number;
    /** 每页显示条目个数 */
    pageSize: number;
    /** 当前页数 */
    currentPage: number;
  };
}

/** 分页查询 */
export interface PageQuery {
  /** 页索引 */
  pageNumber: number;
  /** 每页数量 */
  pageSize: number;
}
