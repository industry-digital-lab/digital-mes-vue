import { httpClient } from "@/api/httpWrapper";

/** 根据当前登录用户，从服务器端获取动态路由表。 */
export const getAsyncRoutes = () => {
  return httpClient.get("/api/route/async-routes");
};
