import { http } from "@/utils/http";
import type { Result, PageResult, PageQuery } from "./types";

/** HTTP 请求包装器 */
class HttpWrapper {
  /** http get 请求 */
  public get<T = any>(url: string, params?: object) {
    return http.request<Result<T>>("get", url, { params });
  }

  /** http get 分页请求 */
  public getPage<T = any>(url: string, params?: PageQuery) {
    return http.request<PageResult<T>>("get", url, { params });
  }

  /** http post 请求 */
  public post<T = any>(url: string, data?: object) {
    return http.request<Result<T>>("post", url, { data });
  }

  /** http post 请求，Content-Type 为 multipart/form-data */
  public postFormData(url: string, data: any) {
    return http.request<Result>(
      "post",
      url,
      { data },
      {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      }
    );
  }

  /** http put 请求 */
  public put(url: string, data?: object) {
    return http.request<Result>("put", url, { data });
  }

  /** http delete 请求 */
  public delete(url: string, data?: object) {
    return http.request<Result>("delete", url, { data });
  }
}

/** HTTP 请求包装器 */
export const httpClient = new HttpWrapper();
