import { httpClient } from "@/api/httpWrapper";
import type { SysMenu } from "@/api/models/sys_menu";

/** 获取菜单管理列表 */
export const getMenuList = (params?: object) => {
  return httpClient.get<SysMenu[]>("/api/menu/list", params);
};

/** 创建新的菜单 */
export const createMenu = (data?: object) => {
  return httpClient.post("/api/menu", data);
};

/** 修改菜单 */
export const updateMenu = (menuId: number, data?: object) => {
  return httpClient.put(`/api/menu/${menuId}`, data);
};

/** 删除菜单 */
export const deleteMenu = (id: number) => {
  return httpClient.delete(`/api/menu/${id}`);
};
