import { httpClient } from "@/api/httpWrapper";
import type { SysRole } from "@/api/models/sys_role";
import type { PageQuery } from "../types";

/** 获取所有角色列表 */
export const getRoleList = (params?: object) => {
  return httpClient.get<SysRole[]>("/api/role/list", params);
};

/** 获取所有角色分页列表 */
export const getRolePagedList = (params?: PageQuery) => {
  return httpClient.getPage<SysRole>("/api/role/list-page", params);
};

/** 根据roleId，获取对应角色信息，包含菜单映射关系。 */
export const getRoleById = (roleId: number) => {
  return httpClient.get<SysRole>(`/api/role/${roleId}`);
};

/** 新增角色 */
export const createRole = (data?: object) => {
  return httpClient.post("/api/role", data);
};

/** 更新角色 */
export const updateRole = (roleId: number, data?: object) => {
  return httpClient.put(`/api/role/${roleId}`, data);
};

/** 派发菜单给角色 */
export const distributeMenus = (roleId: number, data?: object) => {
  return httpClient.put(`/api/role/distribute-menus/${roleId}`, data);
};

/** 删除角色 */
export const deleteRole = (id: number) => {
  return httpClient.delete(`/api/role/${id}`);
};
