import { httpClient } from "@/api/httpWrapper";
import type { PageQuery } from "../types";

/** 分页查询字典项 */
export const getDictPagedList = (params?: PageQuery) => {
  return httpClient.getPage("/api/dict/list-page", params);
};

/** 创建新的字典项 */
export const createDict = (data?: object) => {
  return httpClient.post("/api/dict", data);
};

/** 修改字典项 */
export const updateDict = (id: number, data?: object) => {
  return httpClient.put(`/api/dict/${id}`, data);
};

/** 删除字典项 */
export const deleteDict = (id: number) => {
  return httpClient.delete(`/api/dict/${id}`);
};

/** 分页查询字典项数据 */
export const getDictDataPagedList = (params?: PageQuery) => {
  return httpClient.getPage("/api/dict/data/list-page", params);
};

/** 创建新的字典项数据 */
export const createDictData = (data?: object) => {
  return httpClient.post("/api/dict/data", data);
};

/** 修改字典项数据 */
export const updateDictData = (id: number, data?: object) => {
  return httpClient.put(`/api/dict/data/${id}`, data);
};

/** 删除字典项数据 */
export const deleteDictData = (id: number) => {
  return httpClient.delete(`/api/dict/data/${id}`);
};
