import { httpClient } from "@/api/httpWrapper";
import type { SysOrg } from "@/api/models/sys_org";

/** 获取组织结构管理列表 */
export const getOrgList = (params?: object) => {
  return httpClient.get<SysOrg[]>("/api/org/list", params);
};

/** 新增组织结构 */
export const createOrg = (data?: object) => {
  return httpClient.post("/api/org", data);
};

/** 更新组织结构 */
export const updateOrg = (orgId: number, data?: object) => {
  return httpClient.put(`/api/org/${orgId}`, data);
};

/** 删除组织结构 */
export const deleteOrg = (id: number) => {
  return httpClient.delete(`/api/org/${id}`);
};
