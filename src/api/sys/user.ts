import { httpClient } from "@/api/httpWrapper";
import type { UserInfo } from "@/api/models/sys_user";
import type { PageQuery } from "../types";

/** 获取用户管理分页列表 */
export const getUserPagedList = (params?: PageQuery) => {
  return httpClient.getPage<UserInfo>("/api/user/list-page", params);
};

/** 通过用户Id查询用户，返回包含用户角色对应关系 */
export const getUserById = (userId: number) => {
  return httpClient.get<UserInfo>(`/api/user/${userId}`);
};

/** 获取当前登录用户信息 */
export const getMine = () => {
  return httpClient.get<UserInfo>(`/api/user/mine`);
};

/** 创建用户 */
export const createUser = (data: object) => {
  return httpClient.post(`/api/user/`, data);
};

/** 更新用户 */
export const updateUser = (data: object) => {
  return httpClient.put(`/api/user/`, data);
};

/** 重置用户密码 */
export const resetPassword = (userId: number, data: object) => {
  return httpClient.put(`/api/user/reset-password/${userId}`, data);
};

/** 更改用户状态 */
export const changeStatus = (userId: number, data: object) => {
  return httpClient.put(`/api/user/change-status/${userId}`, data);
};

/** 派发角色给用户 */
export const distributeRoles = (userId: number, data: object) => {
  return httpClient.put(`/api/user/distribute-roles/${userId}`, data);
};

/** 删除用户 */
export const deleteUser = (userId: number) => {
  return httpClient.delete(`/api/user/${userId}`);
};

/** 文件上传 */
export const formUpload = (data: any) => {
  return httpClient.postFormData("", data);
};
