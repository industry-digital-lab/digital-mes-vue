import { RectNode, RectNodeModel, h } from "@logicflow/core";
import { getTextLengthByCanvas } from "../tools/util";

class NormalNodeModel extends RectNodeModel {
  /**
   * 初始化
   */
  initNodeData(data) {
    super.initNodeData(data);
    this.width = 120;
    this.height = 40;
    this.radius = 5;
    this.text.x = this.x;
    this.text.y = this.y;
    this.iconPosition = "";
    this.defaultFill = "#dae8fc";
  }

  getData() {
    const data = super.getData();
    data.properties.ui = "node-normal";
    return data;
  }

  /**
   * 动态设置初始化数据
   */
  setAttributes() {
    if (this.text.value) {
      let width = 30 + getTextLengthByCanvas(this.text.value, 12).width;
      width = Math.ceil(width / 20) * 20;
      if (width < 100) {
        width = 100;
      }
      this.width = width;
    }
  }

  /** 修改节点文本内容 */
  updateText(val) {
    super.updateText(val);
    this.setAttributes();
  }

  /**
   * 重写节点样式
   */
  getNodeStyle() {
    const style = super.getNodeStyle();
    const dataStyle = this.properties.style || {};
    const strokeWidth = Number(dataStyle.strokeWidth) || 1;
    if (this.isSelected) {
      style.strokeWidth = strokeWidth + 1;
      style.stroke = dataStyle.stroke || "#6c8ebf";
    } else {
      style.strokeWidth = strokeWidth;
      style.stroke = dataStyle.stroke || "#999";
    }
    style.fill = dataStyle.fill || this.defaultFill;
    return style;
  }

  /**
   * 重写定义锚点
   */
  getDefaultAnchor() {
    const { x, y, id, width } = this;
    const anchors = [
      {
        x: x + width / 2,
        y: y,
        id: `${id}_right`,
        type: "right"
      },
      {
        x: x - width / 2,
        y: y,
        id: `${id}_left`,
        type: "left"
      }
    ];
    return anchors;
  }

  /**
   *
   */
  getOutlineStyle() {
    const style = super.getOutlineStyle();
    style.stroke = "transparent";
    style.hover.stroke = "transparent";
    return style;
  }
}

class NormalNode extends RectNode {
  getAnchorShape(anchorData) {
    const { x, y } = anchorData;
    return h("rect", {
      x: x - 5,
      y: y - 5,
      width: 10,
      height: 10,
      className: "custom-anchor"
    });
  }

  getShape() {
    const { x, y, width, height, radius } = this.props.model;
    const style = this.props.model.getNodeStyle();
    return h(
      "g",
      {
        className: "lf-normal-node"
      },
      h("rect", {
        ...style,
        x: x - width / 2,
        y: y - height / 2,
        width,
        height,
        rx: radius,
        ry: radius
      })
    );
  }
}

export default {
  type: "normal-node",
  model: NormalNodeModel,
  view: NormalNode
};
