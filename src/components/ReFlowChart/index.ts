import control from "./src/Control.vue";
import nodePanel from "./src/NodePanel.vue";
import dataDialog from "./src/DataDialog.vue";
import NormalLine from "./src/nodes/NormalLine";
import NormalNode from "./src/nodes/NormalNode";
import { withInstall } from "@pureadmin/utils";

import "./index.scss";

/** LogicFlow流程图-控制面板 */
const Control = withInstall(control);

/** LogicFlow流程图-拖拽面板 */
const NodePanel = withInstall(nodePanel);

/** LogicFlow流程图-查看数据 */
const DataDialog = withInstall(dataDialog);

export { Control, NodePanel, DataDialog, NormalLine, NormalNode };

// LogicFlow流程图文档：http://logic-flow.org/
