/** 表单新增或编辑模式枚举 */
export enum EditFormMode {
  /** 新增 */
  add = "新增",
  /** 编辑 */
  edit = "编辑"
}
