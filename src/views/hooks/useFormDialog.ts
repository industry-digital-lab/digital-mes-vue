import { deviceDetection } from "@pureadmin/utils";
import { addDialog } from "@/components/ReDialog";
import { ref, h, type Component } from "vue";

/**
 * 新增表单对话框。
 * @node 对话框中必须暴露 el-form 组件的引用。
 * @param form 要新增的表单
 * @param initData 初始化表单数据，无数据时可传 null，此时表单中可以使用 withDefaults 设置初始值。
 * @param handleAdd 新增处理函数
 * @param afterHandle 新增数据后处理函数
 */
export const openAddFormDialog = <T>(
  form: string | Component,
  initData?: () => T,
  handleAdd?: (item: T) => boolean | Promise<boolean>,
  afterHandle?: (ok?: boolean, item?: T) => void
) => {
  openFormDialog({
    form,
    formItem: initData ? initData() : {},
    handleAdd,
    afterHandle
  });
};

/**
 * 编辑表单对话框。
 * @node 对话框中必须暴露 el-form 组件的引用。
 * @param form 要编辑的表单
 * @param initData 表单初始化的数据
 * @param handleEdit 编辑处理函数
 * @param afterHandle 编辑数据后处理函数
 */
export const openEditFormDialog = <T>(
  form: string | Component,
  initData: () => T,
  handleEdit?: (item: T) => boolean | Promise<boolean>,
  afterHandle?: (ok?: boolean, item?: T) => void
) => {
  openFormDialog({
    isEdit: true,
    form,
    formItem: initData(),
    handleEdit,
    afterHandle
  });
};

/**
 * 打开表单对话框。
 * @node 对话框中必须暴露 el-form 组件的引用，方法如下：
 * @example
 * <template>
 *  <el-form ref="formRef"></el-form>
 * <template>
 * <script setup lang="ts">
 * function getFormRef() {
 *  return formRef.value;
 * }
 * defineExpose({ getFormRef });
 * </script>
 * @param args 参数
 */
export const openFormDialog = <T>(args: {
  isEdit?: boolean;
  title?: string;
  width?: string | number;
  useValidate?: boolean;
  preventOnFail?: boolean;
  form: string | Component;
  formItem: T;
  handleAdd?: (item: T) => boolean | Promise<boolean>;
  handleEdit?: (item: T) => boolean | Promise<boolean>;
  afterHandle?: (ok?: boolean, item?: T) => void;
}) => {
  const dialogFormRef = ref();
  addDialog({
    title: args.title ?? args.isEdit ? "编辑" : "新增",
    props: args.formItem,
    width: args.width ?? "42%",
    draggable: true,
    fullscreen: deviceDetection(),
    fullscreenIcon: true,
    closeOnClickModal: false,
    contentRenderer: () => h(args.form, { ref: dialogFormRef }),
    beforeSure: (done, { options }) => {
      const handle2 = (result: boolean, data: T) => {
        if (!result && ((args.preventOnFail ?? true) || args.preventOnFail)) {
          // 当有设置在执行结果不成功时阻止后续动作
          return;
        }

        if (result) done(); // 关闭弹框
        if (args.afterHandle) args.afterHandle(result, data);
      };

      const handle = () => {
        const curData = options.props as T;
        let result: boolean | Promise<boolean> = true;
        if (!args.isEdit) {
          if (args.handleAdd) result = args.handleAdd(curData);
        } else {
          if (args.handleEdit) result = args.handleEdit(curData);
        }

        if (result instanceof Promise) {
          result.then(res => {
            handle2(res, curData);
          });
        } else {
          handle2(result, curData);
        }
      };

      // 表单弹出框中需要设定 <el-form /> form 表单，且通过 defineExpose 暴露 form 的 ref 引用。
      if ((args.useValidate ?? true) || args.useValidate) {
        const formRef = dialogFormRef.value.getFormRef();
        formRef.validate(valid => {
          if (valid) handle();
        });
      } else {
        handle();
      }
    }
  });
};
