import type { PaginationProps } from "@pureadmin/table";
import type { PageQuery } from "@/api/types";

/**
 * 获取分页查询参数
 * @param pagination 分页属性
 * @param searchParams 查询对象
 * @returns 分页查询参数对象
 */
export const getPageQueryParams = (
  pagination: PaginationProps,
  searchParams?: object
): PageQuery => {
  return {
    ...{ pageNumber: pagination.currentPage, pageSize: pagination.pageSize },
    ...searchParams
  };
};
