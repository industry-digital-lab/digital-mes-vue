import { handleTree } from "@/utils/tree";
import { message } from "@/utils/message";
import { transformI18n } from "@/plugins/i18n";
import { reactive, ref, onMounted, h } from "vue";
import {
  openAddFormDialog,
  openEditFormDialog
} from "@/views/hooks/useFormDialog";
import type { FormProps, FormItemProps } from "./types";
import { useRenderIcon } from "@/components/ReIcon/src/hooks";
import { isAllEmpty } from "@pureadmin/utils";
import {
  getMenuList,
  createMenu,
  updateMenu,
  deleteMenu
} from "@/api/sys/menu";

import editForm from "../form.vue";

export function useMenu() {
  const searchForm = reactive({
    title: ""
  });

  const treeDataList = ref([]);
  const loading = ref(true);

  const getMenuType = (type, text = false) => {
    switch (type) {
      case 0:
        return text ? "菜单" : "primary";
      case 1:
        return text ? "iframe" : "warning";
      case 2:
        return text ? "外链" : "danger";
      case 3:
        return text ? "按钮" : "info";
    }
  };

  const columns: TableColumnList = [
    {
      label: "菜单名称",
      prop: "title",
      align: "left",
      cellRenderer: ({ row }) => (
        <>
          <span class="inline-block mr-1">
            {h(useRenderIcon(row.icon), {
              style: { paddingTop: "1px" }
            })}
          </span>
          <span>{transformI18n(row.title)}</span>
        </>
      )
    },
    {
      label: "菜单类型",
      prop: "menuType",
      width: 100,
      cellRenderer: ({ row, props }) => (
        <el-tag
          size={props.size}
          type={getMenuType(row.menuType)}
          effect="plain"
        >
          {getMenuType(row.menuType, true)}
        </el-tag>
      )
    },
    {
      label: "路由路径",
      prop: "path"
    },
    {
      label: "组件路径",
      prop: "component",
      formatter: ({ path, component }) =>
        isAllEmpty(component) ? path : component
    },
    {
      label: "权限标识",
      prop: "auths"
    },
    {
      label: "排序",
      prop: "rank",
      width: 100
    },
    {
      label: "隐藏",
      prop: "showLink",
      formatter: ({ showLink }) => (showLink ? "否" : "是"),
      width: 100
    },
    {
      label: "操作",
      fixed: "right",
      width: 210,
      slot: "operation"
    }
  ];

  function handleSelectionChange(val: any) {
    console.log("handleSelectionChange", val);
  }

  function resetForm(formEl: any) {
    if (!formEl) return;
    formEl.resetFields();
    onSearch();
  }

  async function onSearch() {
    loading.value = true;
    const { data } = await getMenuList();
    let newData = data;
    if (!isAllEmpty(searchForm.title)) {
      // 前端搜索菜单名称
      newData = newData.filter(item =>
        transformI18n(item.title).includes(searchForm.title)
      );
    }
    treeDataList.value = handleTree(newData); // 处理成树结构
    loading.value = false;
  }

  const handleAdd = (row?: FormItemProps) => {
    openAddFormDialog<FormProps>(
      editForm,
      () => (row ? { formInline: row } : null),
      async (item: FormProps) => {
        const ret = await createMenu(item.formInline);
        if (!ret.success) {
          message(`新增菜单失败，${ret.message}`, { type: "warning" });
          return false;
        }
        message(`新增菜单成功`, { type: "success" });
        return true;
      },
      () => {
        onSearch();
      }
    );
  };

  const handleEdit = (row: FormItemProps) => {
    openEditFormDialog<FormProps>(
      editForm,
      () => ({
        formInline: row
      }),
      async (item: FormProps) => {
        const ret = await updateMenu(item.formInline.id, item.formInline);
        if (!ret.success) {
          message(`更新菜单失败，${ret.message}`, { type: "warning" });
          return false;
        }
        message(`更新菜单成功`, { type: "success" });
        return true;
      },
      () => {
        onSearch();
      }
    );
  };

  async function handleDelete(row: any) {
    const ret = await deleteMenu(row.id);
    if (!ret.success) {
      message(`删除菜单失败，${ret.message}`, { type: "warning" });
      return;
    }
    message(`删除菜单成功`, { type: "success" });
    onSearch();
  }

  onMounted(() => {
    onSearch();
  });

  return {
    searchForm,
    loading,
    columns,
    treeDataList,
    /** 搜索 */
    onSearch,
    /** 重置 */
    resetForm,
    handleAdd,
    handleEdit,
    /** 删除菜单 */
    handleDelete,
    handleSelectionChange
  };
}
