/** 表单项 */
interface FormItemProps {
  /** 机构Id */
  id: number;
  /** 父Id */
  parentId?: number;
  /** 机构名称 */
  name: string;
  /** 机构代码 */
  code: string;
  /** 负责人 */
  principal?: string;
  /** 电话 */
  phone?: string | number;
  /** 邮箱 */
  email?: string;
  /** 排序 */
  sort: number;
  /** 状态：1->启用；0->停用 */
  status: number;
  /** 备注 */
  remark?: string;
}

/** 可编辑表单 */
interface FormProps {
  /** 表单行 */
  formInline: FormItemProps;

  /** 机构树节点选项 */
  treeOrgOptions: Record<string, unknown>[];
}

export type { FormItemProps, FormProps };
