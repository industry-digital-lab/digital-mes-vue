import { isAllEmpty } from "@pureadmin/utils";
import { handleTree, handleTreeNode } from "@/utils/tree";
import { message } from "@/utils/message";
import {
  openAddFormDialog,
  openEditFormDialog
} from "@/views/hooks/useFormDialog";
import { useStyleHooks } from "@/views/hooks/useStyle";
import { reactive, ref, onMounted } from "vue";
import type { FormProps, FormItemProps } from "./types";
import type { SysOrg } from "@/api/models/sys_org";
import { getOrgList, createOrg, updateOrg, deleteOrg } from "@/api/sys/org";

import editForm from "../form.vue";

export function useOrg() {
  const searchForm = reactive({
    name: "",
    status: null
  });

  const treeDataList = ref<SysOrg[]>([]);
  const loading = ref(true);
  const { tagStyle } = useStyleHooks();

  const columns: TableColumnList = [
    {
      label: "部门名称",
      prop: "name",
      width: 260,
      align: "left"
    },
    {
      label: "状态",
      prop: "status",
      minWidth: 100,
      cellRenderer: ({ row, props }) => (
        <el-tag size={props.size} style={tagStyle.value(row.status)}>
          {row.status === 1 ? "启用" : "停用"}
        </el-tag>
      )
    },
    {
      label: "备注",
      prop: "remark",
      minWidth: 320
    },
    {
      label: "创建时间",
      prop: "createdTime",
      minWidth: 140
    },
    {
      label: "操作",
      fixed: "right",
      width: 210,
      slot: "operation"
    }
  ];

  function resetForm(formEl: any) {
    if (!formEl) return;
    formEl.resetFields();
    onSearch();
  }

  async function onSearch() {
    loading.value = true;
    const { data } = await getOrgList();
    let newData = data;
    if (!isAllEmpty(searchForm.name)) {
      // 前端搜索部门名称
      newData = newData.filter(item => item.name.includes(searchForm.name));
    }
    if (!isAllEmpty(searchForm.status)) {
      // 前端搜索状态
      newData = newData.filter(item => item.status === searchForm.status);
    }
    treeDataList.value = handleTree(newData); // 处理成树结构
    loading.value = false;
  }

  const handleAdd = (row?: FormItemProps) => {
    openAddFormDialog<FormProps>(
      editForm,
      () => ({
        formInline: {
          id: row?.id ?? 0,
          parentId: row?.parentId ?? 0,
          name: row?.name ?? "",
          code: row?.code ?? "",
          principal: row?.principal ?? "",
          phone: row?.phone ?? "",
          email: row?.email ?? "",
          sort: row?.sort ?? 0,
          status: row?.status ?? 1,
          remark: row?.remark ?? ""
        },
        treeOrgOptions: handleTreeNode(
          treeDataList.value,
          item => (item.disabled = !item.status)
        )
      }),
      async (item: FormProps) => {
        const ret = await createOrg(item.formInline);
        if (!ret.success) {
          message(`新增部门失败，${ret.message}`, { type: "warning" });
          return false;
        }
        message(`新增部门成功`, { type: "success" });
        return true;
      },
      () => {
        onSearch();
      }
    );
  };

  const handleEdit = (row: FormItemProps) => {
    openEditFormDialog<FormProps>(
      editForm,
      () => ({
        formInline: row,
        treeOrgOptions: handleTreeNode(
          treeDataList.value,
          item => (item.disabled = !item.status)
        )
      }),
      async (item: FormProps) => {
        const ret = await updateOrg(item.formInline.id, item.formInline);
        if (!ret.success) {
          message(`更新部门失败，${ret.message}`, { type: "warning" });
          return false;
        }
        message(`更新部门成功`, { type: "success" });
        return true;
      },
      () => {
        onSearch();
      }
    );
  };

  async function handleDelete(row: any) {
    const ret = await deleteOrg(row.id);
    if (!ret.success) {
      message(`删除部门失败，${ret.message}`, { type: "warning" });
      return;
    }
    message(`删除部门成功`, { type: "success" });
    onSearch();
  }

  onMounted(() => {
    onSearch();
  });

  return {
    searchForm,
    loading,
    columns,
    treeDataList,
    /** 搜索 */
    onSearch,
    /** 重置 */
    resetForm,
    handleAdd,
    handleEdit,
    /** 删除部门 */
    handleDelete
  };
}
