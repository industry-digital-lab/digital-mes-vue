interface FormItemProps {
  id: number;
  /** 角色名称 */
  name: string;
  /** 角色编号 */
  code: string;
  /** 排序 */
  sort: number;
  /** 状态 */
  status: number;
  /** 备注 */
  remark?: string;
}

interface FormProps {
  formInline: FormItemProps;
}

export type { FormItemProps, FormProps };
