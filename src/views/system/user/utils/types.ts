interface FormItemProps {
  id: number;
  userName: string;
  password: string;
  nickname: string;
  phone?: string | number;
  email?: string;
  avatar?: string;
  gender: number;
  orgId?: number;
  org?: {
    id: number;
    name: string;
  };
  status: number;
}

interface FormProps {
  formInline: FormItemProps;
  treeOrgOptions: Record<string, unknown>[];
}

interface RoleFormItemProps {
  userName: string;
  nickname: string;
  /** 角色列表 */
  roleOptions: any[];
  /** 选中的角色列表 */
  ids: number[];
}

interface RoleFormProps {
  formInline: RoleFormItemProps;
}

export type { FormItemProps, FormProps, RoleFormItemProps, RoleFormProps };
