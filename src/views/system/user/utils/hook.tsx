import "./reset.css";
import roleForm from "../form/role.vue";
import editForm from "../form/index.vue";
import { zxcvbn } from "@zxcvbn-ts/core";
import { handleTree, handleTreeNode } from "@/utils/tree";
import { message } from "@/utils/message";
import userAvatar from "@/assets/avatar.png";
import { useStyleHooks } from "@/views/hooks/useStyle";
import {
  openAddFormDialog,
  openEditFormDialog,
  openFormDialog
} from "@/views/hooks/useFormDialog";
import { getPageQueryParams } from "@/views/hooks/useWebRequest";
import { addDialog } from "@/components/ReDialog";
import type { PaginationProps } from "@pureadmin/table";
import type { FormProps, FormItemProps, RoleFormProps } from "./types";
import {
  getKeyList,
  isAllEmpty,
  hideTextAtIndex,
  deviceDetection
} from "@pureadmin/utils";
import { getOrgList } from "@/api/sys/org";
import { getRoleList } from "@/api/sys/role";
import {
  getUserPagedList,
  getUserById,
  createUser,
  updateUser,
  resetPassword,
  changeStatus,
  distributeRoles,
  deleteUser
} from "@/api/sys/user";
import {
  ElForm,
  ElInput,
  ElFormItem,
  ElProgress,
  ElMessageBox
} from "element-plus";
import { type Ref, ref, watch, computed, reactive, onMounted } from "vue";

export function useUser(tableRef: Ref, treeRef: Ref) {
  const searchForm = reactive({
    // 左侧部门树的id
    orgId: 0,
    userName: "",
    status: ""
  });
  const ruleFormRef = ref();
  const dataList = ref([]);
  const loading = ref(true);
  const switchLoadMap = ref<{ [key: number]: boolean }>({});
  const { switchStyle } = useStyleHooks();
  const treeData = ref([]);
  const treeLoading = ref(true);
  const selectedNum = ref(0);
  const pagination = reactive<PaginationProps>({
    total: 0,
    pageSize: 10,
    currentPage: 1,
    background: true
  });
  const columns: TableColumnList = [
    {
      label: "勾选列", // 如果需要表格多选，此处label必须设置
      type: "selection",
      fixed: "left",
      reserveSelection: true // 数据刷新后保留选项
    },
    {
      label: "id",
      prop: "id",
      hide: true
    },
    {
      label: "用户账号",
      prop: "userName",
      minWidth: 130
    },
    {
      label: "用户昵称",
      prop: "nickname",
      minWidth: 130
    },
    {
      label: "用户头像",
      prop: "avatar",
      cellRenderer: ({ row }) => (
        <el-image
          fit="cover"
          preview-teleported={true}
          src={row.avatar || userAvatar}
          preview-src-list={Array.of(row.avatar || userAvatar)}
          class="w-[24px] h-[24px] rounded-full align-middle"
        />
      ),
      width: 90
    },
    {
      label: "性别",
      prop: "gender",
      minWidth: 90,
      cellRenderer: ({ row, props }) => (
        <el-tag
          size={props.size}
          type={row.gender === 2 ? "danger" : null}
          effect="plain"
        >
          {row.gender === 1 ? "男" : "女"}
        </el-tag>
      )
    },
    {
      label: "部门",
      prop: "org.name",
      minWidth: 90
    },
    {
      label: "手机号码",
      prop: "phone",
      minWidth: 90,
      formatter: ({ phone }) => hideTextAtIndex(phone, { start: 3, end: 6 })
    },
    {
      label: "状态",
      prop: "status",
      minWidth: 90,
      cellRenderer: scope => (
        <el-switch
          size={scope.props.size === "small" ? "small" : "default"}
          loading={switchLoadMap.value[scope.row.id]}
          v-model={scope.row.status}
          active-value={1}
          inactive-value={0}
          active-text="已启用"
          inactive-text="已停用"
          inline-prompt
          style={switchStyle.value}
          onChange={() => onChangeStatus(scope as any)}
        />
      )
    },
    {
      label: "创建时间",
      prop: "createdTime",
      minWidth: 90
    },
    {
      label: "操作",
      fixed: "right",
      width: 180,
      slot: "operation"
    }
  ];
  const buttonClass = computed(() => {
    return [
      "!h-[20px]",
      "reset-margin",
      "!text-gray-500",
      "dark:!text-white",
      "dark:hover:!text-primary"
    ];
  });
  // 重置的新密码
  const pwdForm = reactive({
    newPwd: ""
  });
  const pwdProgress = [
    { color: "#e74242", text: "非常弱" },
    { color: "#EFBD47", text: "弱" },
    { color: "#ffa500", text: "一般" },
    { color: "#1bbf1b", text: "强" },
    { color: "#008000", text: "非常强" }
  ];
  // 当前密码强度（0-4）
  const curScore = ref();
  const roleOptions = ref([]);

  /** 更改用户状态 */
  function onChangeStatus({ row }) {
    ElMessageBox.confirm(
      `确认要<strong>${
        row.status === 0 ? "停用" : "启用"
      }</strong><strong style='color:var(--el-color-primary)'>${
        row.userName
      }</strong>用户吗?`,
      "系统提示",
      {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning",
        dangerouslyUseHTMLString: true,
        draggable: true
      }
    )
      .then(async () => {
        switchLoadMap.value[row.id] = true;
        const ret = await changeStatus(row.id, { status: row.status });
        if (ret.success) {
          message("已成功修改用户状态", {
            type: "success"
          });
        }

        switchLoadMap.value[row.id] = false;
      })
      .catch(() => {
        switchLoadMap.value[row.id] = false;
        row.status === 0 ? (row.status = 1) : (row.status = 0);
      });
  }

  function handleSizeChange(val: number) {
    console.log(`${val} items per page`);
  }

  function handleCurrentChange(val: number) {
    console.log(`current page: ${val}`);
  }

  /** 当CheckBox选择项发生变化时会触发该事件 */
  function handleSelectionChange(val) {
    selectedNum.value = val.length;
    // 重置表格高度
    tableRef.value.setAdaptive();
  }

  /** 取消选择 */
  function onSelectionCancel() {
    selectedNum.value = 0;
    // 用于多选表格，清空用户的选择
    tableRef.value.getTableRef().clearSelection();
  }

  /** 批量删除 */
  function onbatchDel() {
    // 返回当前选中的行
    const curSelected = tableRef.value.getTableRef().getSelectionRows();
    // 接下来根据实际业务，通过选中行的某项数据，比如下面的id，调用接口进行批量删除
    message(`已删除用户编号为 ${getKeyList(curSelected, "id")} 的数据`, {
      type: "success"
    });
    tableRef.value.getTableRef().clearSelection();
    onSearch();
  }

  async function onSearch() {
    loading.value = true;
    const params = getPageQueryParams(pagination, searchForm);
    const { data } = await getUserPagedList(params);
    dataList.value = data.items;
    pagination.total = data.totalCount;

    loading.value = false;
  }

  const resetForm = formEl => {
    if (!formEl) return;
    formEl.resetFields();
    searchForm.orgId = 0;
    treeRef.value.onTreeReset();
    onSearch();
  };

  function onTreeSelect({ id, selected }) {
    searchForm.orgId = selected ? id : 0;
    onSearch();
  }

  const handleAdd = () => {
    openAddFormDialog<FormProps>(
      editForm,
      () => ({
        formInline: { gender: 1, status: 1 } as FormItemProps,
        treeOrgOptions: handleTreeNode(
          treeData.value,
          item => (item.disabled = !item.status)
        )
      }),
      async (item: FormProps) => {
        const ret = await createUser(item.formInline);
        if (!ret.success) {
          message(`新增用户失败，${ret.message}`, { type: "warning" });
          return false;
        }
        message(`新增用户成功`, { type: "success" });
        return true;
      },
      () => {
        onSearch();
      }
    );
  };

  const handleEdit = (row: FormItemProps) => {
    openEditFormDialog<FormProps>(
      editForm,
      () => ({
        formInline: row,
        treeOrgOptions: handleTreeNode(
          treeData.value,
          item => (item.disabled = !item.status)
        )
      }),
      async (item: FormProps) => {
        const ret = await updateUser(item.formInline);
        if (!ret.success) {
          message(`更新用户失败，${ret.message}`, { type: "warning" });
          return false;
        }
        message(`更新用户成功`, { type: "success" });
        return true;
      },
      () => {
        onSearch();
      }
    );
  };

  async function handleDelete(row: FormItemProps) {
    await deleteUser(row.id);
    onSearch();
  }

  /** 重置密码 */
  function handleReset(row: FormItemProps) {
    addDialog({
      title: `重置 ${row.userName} 用户的密码`,
      width: "30%",
      draggable: true,
      closeOnClickModal: false,
      fullscreen: deviceDetection(),
      contentRenderer: () => (
        <>
          <ElForm ref={ruleFormRef} model={pwdForm}>
            <ElFormItem
              prop="newPwd"
              rules={[
                {
                  required: true,
                  message: "请输入新密码",
                  trigger: "blur"
                }
              ]}
            >
              <ElInput
                clearable
                show-password
                type="password"
                v-model={pwdForm.newPwd}
                placeholder="请输入新密码"
              />
            </ElFormItem>
          </ElForm>
          <div class="mt-4 flex">
            {pwdProgress.map(({ color, text }, idx) => (
              <div
                class="w-[19vw]"
                style={{ marginLeft: idx !== 0 ? "4px" : 0 }}
              >
                <ElProgress
                  striped
                  striped-flow
                  duration={curScore.value === idx ? 6 : 0}
                  percentage={curScore.value >= idx ? 100 : 0}
                  color={color}
                  stroke-width={10}
                  show-text={false}
                />
                <p
                  class="text-center"
                  style={{ color: curScore.value === idx ? color : "" }}
                >
                  {text}
                </p>
              </div>
            ))}
          </div>
        </>
      ),
      closeCallBack: () => (pwdForm.newPwd = ""),
      beforeSure: done => {
        ruleFormRef.value.validate(async (valid: any) => {
          if (valid) {
            const ret = await resetPassword(row.id, {
              newPassword: pwdForm.newPwd
            });
            if (!ret.success) {
              message(`重置密码失败，${ret.message}`, { type: "warning" });
              return;
            }

            message(`已成功重置 ${row.userName} 用户的密码`, {
              type: "success"
            });
            done(); // 关闭弹框
            onSearch(); // 刷新表格数据
          }
        });
      }
    });
  }

  /** 分配角色 */
  async function handleRole(row) {
    // 选中的角色列表
    const user = await getUserById(row.id);
    const ids = user.data?.userRoles.map(s => s.roleId) ?? [];

    openFormDialog<RoleFormProps>({
      title: `分配 ${row.userName} 用户的角色`,
      width: "400px",
      useValidate: false,
      form: roleForm,
      formItem: {
        formInline: {
          userName: row?.userName ?? "",
          nickname: row?.nickname ?? "",
          roleOptions: roleOptions.value ?? [],
          ids: ids
        }
      },
      handleAdd: async (item: RoleFormProps) => {
        console.log(item);
        const ret = await distributeRoles(row.id, {
          userId: row.id,
          roleIds: item.formInline.ids
        });
        if (!ret.success) {
          message(`分配角色失败，${ret.message}`, { type: "warning" });
          return false;
        }
        message(`分配角色成功`, { type: "success" });
        return true;
      }
    });
  }

  onMounted(async () => {
    treeLoading.value = true;
    onSearch();

    // 归属部门
    const { data } = await getOrgList();
    treeData.value = handleTree(data);
    treeLoading.value = false;

    // 角色列表
    const roles = await getRoleList();
    roleOptions.value = roles.data;
  });

  watch(
    pwdForm,
    ({ newPwd }) =>
      (curScore.value = isAllEmpty(newPwd) ? -1 : zxcvbn(newPwd).score)
  );

  return {
    searchForm,
    loading,
    columns,
    dataList,
    treeData,
    treeLoading,
    selectedNum,
    pagination,
    buttonClass,
    deviceDetection,
    onSearch,
    resetForm,
    onbatchDel,
    onTreeSelect,
    handleAdd,
    handleEdit,
    handleDelete,
    handleReset,
    handleRole,
    handleSizeChange,
    onSelectionCancel,
    handleCurrentChange,
    handleSelectionChange
  };
}
