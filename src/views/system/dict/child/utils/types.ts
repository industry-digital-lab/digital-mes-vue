/** 字典数据属性 */
interface FormItemProps {
  id: number;
  dictId: number;
  label: string;
  value: string;
  isFixed: boolean;
  sort: number;
  status: number;
  remark?: string;
}

/** 字典数据条目属性 */
interface FormProps {
  formInline: FormItemProps;
}

export type { FormItemProps, FormProps };
