import type { FormRules } from "element-plus";
import { reactive } from "vue";

export const formRules = reactive(<FormRules>{
  label: [{ required: true, message: "请输入字典标签", trigger: "blur" }],
  value: [{ required: true, message: "请输入字典健值", trigger: "blur" }]
});
