import { message } from "@/utils/message";
import { useStyleHooks } from "@/views/hooks/useStyle";
import {
  openAddFormDialog,
  openEditFormDialog
} from "@/views/hooks/useFormDialog";
import type { FormProps, FormItemProps } from "./types";
import { getPageQueryParams } from "@/views/hooks/useWebRequest";
import type { PaginationProps } from "@pureadmin/table";
import { reactive, ref } from "vue";
import {
  getDictDataPagedList,
  createDictData,
  updateDictData,
  deleteDictData
} from "@/api/sys/dict";
import editForm from "../form.vue";

export function useDictData() {
  const { tagStyle } = useStyleHooks();

  const dictId = ref(0);
  const dataList = ref([]);
  const loading = ref(false);

  const pagination = reactive<PaginationProps>({
    total: 0,
    pageSize: 10,
    currentPage: 1,
    background: true
  });

  const columns: TableColumnList = [
    {
      label: "字典标签",
      prop: "label",
      width: 160
    },
    {
      label: "字典键值",
      prop: "value",
      width: 160
    },
    {
      label: "排序",
      prop: "sort",
      width: 80
    },
    {
      label: "状态",
      prop: "status",
      width: 80,
      cellRenderer: ({ row, props }) => (
        <el-tag size={props.size} style={tagStyle.value(row.status)}>
          {row.status === 1 ? "启用" : "停用"}
        </el-tag>
      )
    },
    {
      label: "备注",
      prop: "remark"
    },
    {
      label: "操作",
      fixed: "right",
      width: 80,
      slot: "operation"
    }
  ];

  async function onSearch() {
    loading.value = true;
    const params = getPageQueryParams(pagination, { dictId: dictId.value });
    const { data } = await getDictDataPagedList(params);
    dataList.value = data.items;
    pagination.total = data.totalCount;

    loading.value = false;
  }

  /** 新增字典 */
  const handleAdd = () => {
    openAddFormDialog<FormProps>(
      editForm,
      null,
      async (item: FormProps) => {
        const ret = await createDictData(item.formInline);
        if (!ret.success) {
          message(`新增字典失败，${ret.message}`, { type: "warning" });
          return false;
        }
        message(`新增字典成功`, { type: "success" });
        return true;
      },
      () => {
        onSearch();
      }
    );
  };

  /** 编辑字典 */
  const handleEdit = (row: FormItemProps) => {
    openEditFormDialog<FormProps>(
      editForm,
      () => ({
        formInline: row
      }),
      async (item: FormProps) => {
        const ret = await updateDictData(item.formInline.id, item.formInline);
        if (!ret.success) {
          message(`更新字典失败，${ret.message}`, { type: "warning" });
          return false;
        }
        message(`更新字典成功`, { type: "success" });
        return true;
      },
      () => {
        onSearch();
      }
    );
  };

  /** 删除字典 */
  const handleDelete = async (row: FormItemProps) => {
    const ret = await deleteDictData(row.id);
    if (!ret.success) {
      message(`删除字典失败，${ret.message}`, { type: "warning" });
      return;
    }
    message(`删除字典成功`, { type: "success" });
    onSearch();
  };

  function handleChangeCurrentPage(val: number) {
    pagination.currentPage = val;
    onSearch();
  }

  function handleChangePageSize(val: number) {
    pagination.pageSize = val;
    onSearch();
  }

  function reloadData() {
    if (dictId.value > 0) {
      onSearch();
    } else {
      dataList.value = [];
      pagination.total = 0;
    }
  }

  return {
    loading,
    dictId,
    pagination,
    columns,
    dataList,
    reloadData,
    handleAdd,
    handleEdit,
    handleDelete,
    handleChangeCurrentPage,
    handleChangePageSize
  };
}
