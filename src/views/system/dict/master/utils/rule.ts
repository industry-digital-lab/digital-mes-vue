import type { FormRules } from "element-plus";
import { reactive } from "vue";

export const formRules = reactive(<FormRules>{
  name: [{ required: true, message: "请输入字典名称", trigger: "blur" }],
  code: [{ required: true, message: "请输入字典编号", trigger: "blur" }]
});
