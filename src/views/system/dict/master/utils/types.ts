/** 字典项属性 */
interface FormItemProps {
  id: number;
  name: string;
  code: string;
  status: number;
  isPrimitive: boolean;
  remark?: string;
}

/** 字典属性 */
interface FormProps {
  formInline: FormItemProps;
}

export type { FormItemProps, FormProps };
