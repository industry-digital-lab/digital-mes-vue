import { reactive } from "vue";
import type { FormRules } from "element-plus";

/** 自定义表单规则校验 */
export const formRules = reactive(<FormRules>{
  lineCode: [{ required: true, message: "产线编号为必填项", trigger: "blur" }],
  lineName: [{ required: true, message: "产线名称为必填项", trigger: "blur" }]
});
