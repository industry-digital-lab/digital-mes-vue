import { useStyleHooks } from "@/views/hooks/useStyle";
import {
  openAddFormDialog,
  openEditFormDialog
} from "@/views/hooks/useFormDialog";
import type { FormProps, FormItemProps } from "./types";
import { reactive, ref } from "vue";

import editForm from "../form.vue";

export function useLine() {
  const { tagStyle } = useStyleHooks();

  const searchForm = reactive({
    name: ""
  });
  const dataList = ref([]);
  const loading = ref(false);

  const columns: TableColumnList = [
    {
      label: "产线编号",
      prop: "lineCode",
      width: 180
    },
    {
      label: "产线名称",
      prop: "lineName",
      width: 220
    },
    {
      label: "负责人",
      prop: "principal",
      width: 120
    },
    {
      label: "状态",
      prop: "status",
      width: 80,
      cellRenderer: ({ row, props }) => (
        <el-tag size={props.size} style={tagStyle.value(row.status)}>
          {row.status === 1 ? "启用" : "停用"}
        </el-tag>
      )
    },
    {
      label: "备注",
      prop: "remark"
    },
    {
      label: "操作",
      fixed: "right",
      width: 180,
      slot: "operation"
    }
  ];

  async function onSearch() {
    loading.value = true;

    loading.value = false;
  }

  const resetForm = (formEl: any) => {
    if (!formEl) return;
    formEl.resetFields();
    onSearch();
  };

  /** 新增 */
  const handleAdd = () => {
    openAddFormDialog<FormProps>(
      editForm,
      null,
      async (item: FormProps) => {
        console.log(item);
        return true;
      },
      () => {
        onSearch();
      }
    );
  };

  /** 编辑 */
  const handleEdit = (row: FormItemProps) => {
    openEditFormDialog<FormProps>(
      editForm,
      () => ({
        formInline: row
      }),
      async (item: FormProps) => {
        console.log(item);
        return true;
      },
      () => {
        onSearch();
      }
    );
  };

  /** 删除 */
  const handleDelete = async (row: FormItemProps) => {
    console.log(row.id);
    onSearch();
  };

  return {
    searchForm,
    loading,
    columns,
    dataList,
    resetForm,
    onSearch,
    handleAdd,
    handleEdit,
    handleDelete
  };
}
