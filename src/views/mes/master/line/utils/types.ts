/** 产线项属性 */
interface FormItemProps {
  id: number;
  lineCode: string;
  lineName: string;
  principal?: string;
  remark?: string;
  status: number;
}

/** 产线属性 */
interface FormProps {
  formInline: FormItemProps;
}

export type { FormItemProps, FormProps };
