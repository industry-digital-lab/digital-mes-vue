import type LogicFlow from "@logicflow/core";
import { ref, unref } from "vue";

/** 节点样式类型 */
export type NodeStyleType = {
  fill?: string;
  stroke?: string;
  strokeWidth?: number;
};

export function useFlowChart() {
  const nodeStyle: Record<string, NodeStyleType> = {
    /** 正常 */
    normal: {
      fill: "#dae8fc",
      stroke: "#6c8ebf"
    },
    /** 可选 */
    optional: {
      fill: "rgb(253, 208, 162)",
      stroke: "#ff7f0e"
    },
    /** 禁用 */
    disabled: {
      fill: "rgb(248, 206, 204)",
      stroke: "#b85450"
    }
  };

  const loading = ref(false);
  const lf = ref<LogicFlow>();

  function getNodeList(lineId: number = 0): any[] {
    console.log(lineId);

    function padLeft(str: string, len: number): string {
      let str2 = str;
      while (str2.length < len) {
        str2 = "0" + str2;
      }

      return str2;
    }

    const nodeList = [];
    for (let index = 1; index <= 20; index++) {
      nodeList.push({
        id: `OP${padLeft(index.toString(), 3)}`,
        text: `OP${padLeft(index.toString(), 3)}`,
        type: "normal-node"
      });
    }

    return nodeList;
  }

  function getGraphData(routeId: number = 0): any {
    console.log(routeId);
    return {};
  }

  function handleSave() {
    loading.value = true;
    const graphData = unref(lf).getGraphData();

    const input = { routeId: 0, graph: JSON.stringify(graphData) };
    console.log("input", input);

    loading.value = false;
  }

  return {
    /** 节点样式 */
    nodeStyle,
    loading,
    /** LogicFlow 节点 */
    lf,
    /** 获取工艺路线节点 */
    getNodeList,
    /** 获取工艺路线图数据 */
    getGraphData,
    /** 保存工艺路线图 */
    handleSave
  };
}
