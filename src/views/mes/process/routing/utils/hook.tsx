import { useStyleHooks } from "@/views/hooks/useStyle";
import {
  openAddFormDialog,
  openEditFormDialog
} from "@/views/hooks/useFormDialog";
import type { FormProps, FormItemProps } from "./types";
import { useMultiTagsStoreHook } from "@/store/modules/multiTags";
import { useRouter } from "vue-router";
import { reactive, ref } from "vue";

import editForm from "../form.vue";

export function useProcessRoute() {
  const router = useRouter();

  const { tagStyle } = useStyleHooks();

  const searchForm = reactive({
    lineId: undefined
  });

  const dataList = ref([]);
  const loading = ref(false);

  const columns: TableColumnList = [
    {
      label: "产线名称",
      prop: "lineName",
      width: 220
    },
    {
      label: "路线代码",
      prop: "routeCode",
      width: 180
    },
    {
      label: "路线名称",
      prop: "routeName",
      width: 220
    },
    {
      label: "状态",
      prop: "status",
      width: 120,
      cellRenderer: ({ row, props }) => (
        <el-tag size={props.size} style={tagStyle.value(row.status)}>
          {row.status === 1 ? "启用" : "停用"}
        </el-tag>
      )
    },
    {
      label: "备注",
      prop: "remark"
    },
    {
      label: "操作",
      fixed: "right",
      width: 280,
      slot: "operation"
    }
  ];

  async function getLineOptions() {
    return [
      { label: "生产线01", value: 1001 },
      { label: "生产线02", value: 1002 },
      { label: "生产线03", value: 1003 }
    ];
  }

  async function onSearch() {
    loading.value = true;
    dataList.value = [
      {
        lineId: 1001,
        lineName: "生产线01",
        id: 11001,
        routeCode: "R11001",
        routeName: "工艺路线01",
        status: 1,
        remark: ""
      },
      {
        lineId: 1001,
        lineName: "生产线01",
        id: 11002,
        routeCode: "R11002",
        routeName: "工艺路线02",
        status: 1,
        remark: ""
      },
      {
        lineId: 1002,
        lineName: "生产线02",
        id: 11003,
        routeCode: "R11003",
        routeName: "工艺路线03",
        status: 1,
        remark: ""
      },
      {
        lineId: 1002,
        lineName: "生产线02",
        id: 11004,
        routeCode: "R11004",
        routeName: "工艺路线04",
        status: 1,
        remark: ""
      },
      {
        lineId: 1003,
        lineName: "生产线03",
        id: 11005,
        routeCode: "R11005",
        routeName: "工艺路线05",
        status: 1,
        remark: ""
      }
    ];
    loading.value = false;
  }

  const resetForm = (formEl: any) => {
    if (!formEl) return;
    formEl.resetFields();
    onSearch();
  };

  /** 新增 */
  const handleAdd = () => {
    openAddFormDialog<FormProps>(
      editForm,
      null,
      async (item: FormProps) => {
        console.log(item);
        return true;
      },
      () => {
        onSearch();
      }
    );
  };

  /** 编辑 */
  const handleEdit = (row: FormItemProps) => {
    openEditFormDialog<FormProps>(
      editForm,
      () => ({
        formInline: row
      }),
      async (item: FormProps) => {
        console.log(item);
        return true;
      },
      () => {
        onSearch();
      }
    );
  };

  /** 删除 */
  const handleDelete = async (row: FormItemProps) => {
    console.log(row);
    onSearch();
  };

  /** 链接 */
  const handleLink = (row: FormItemProps) => {
    const parameter = { id: row.id.toString() };
    useMultiTagsStoreHook().handleTags("push", {
      path: `/mes/process/routeflow/index`,
      name: "MesProcessRouteFlow",
      query: parameter,
      meta: {
        title: {
          zh: `工艺路线流程图`,
          en: `工艺路线流程图`
        },
        // 最大打开标签数
        dynamicLevel: 1
      }
    });
    router.push({ name: "MesProcessRouteFlow", query: parameter });
  };

  return {
    searchForm,
    loading,
    columns,
    dataList,
    resetForm,
    getLineOptions,
    onSearch,
    handleAdd,
    handleEdit,
    handleDelete,
    handleLink
  };
}
