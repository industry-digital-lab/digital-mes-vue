import type { FormRules } from "element-plus";
import { reactive } from "vue";

export const formRules = reactive(<FormRules>{
  routeCode: [
    { required: true, message: "请输入工艺路线代码", trigger: "blur" }
  ],
  routeName: [
    { required: true, message: "请输入工艺路线名称", trigger: "blur" }
  ],
  lineId: [{ required: true, message: "请选择生产线", trigger: "blur" }]
});
