/** 数据属性 */
interface FormItemProps {
  id: number;
  lineId: number;
  routeCode: string;
  routeName: string;
  status: number;
  remark?: string;
}

/** 数据条目属性 */
interface FormProps {
  formInline: FormItemProps;
}

export type { FormItemProps, FormProps };
