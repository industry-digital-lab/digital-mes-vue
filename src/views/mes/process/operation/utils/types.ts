/** 数据属性 */
interface FormItemProps {
  id: number;
  opCode?: string;
  opName?: string;
  lineId?: number;
  lineCode?: string;
  workstations?: [];
  esop?: string;
  classify?: number;
  procTypeCodes?: any[];
  procTypeNames?: any[];
  sort?: number;
  status: number;
}

/** 数据条目属性 */
interface FormProps {
  formInline: FormItemProps;
}

export type { FormItemProps, FormProps };
