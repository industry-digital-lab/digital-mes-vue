import { useStyleHooks } from "@/views/hooks/useStyle";
import {
  openAddFormDialog,
  openEditFormDialog
} from "@/views/hooks/useFormDialog";
import type { FormProps, FormItemProps } from "./types";
import { ref } from "vue";

import editForm from "../form.vue";

export function useProcessOperation() {
  const { tagStyle } = useStyleHooks();

  const dataList = ref([]);
  const loading = ref(false);

  const columns: TableColumnList = [
    {
      label: "工序编码",
      prop: "opCode",
      width: 120
    },
    {
      label: "工序名称",
      prop: "opName",
      width: 140
    },
    {
      label: "工位集合",
      prop: "workstations",
      width: 160
    },
    {
      label: "分类",
      prop: "classify",
      width: 160
    },
    {
      label: "工序类型",
      prop: "procTypeNames",
      width: 160
    },
    {
      label: "创建时间",
      prop: "createdTime",
      minWidth: 140
    },
    {
      label: "状态",
      prop: "status",
      width: 120,
      cellRenderer: ({ row, props }) => (
        <el-tag size={props.size} style={tagStyle.value(row.status)}>
          {row.status === 1 ? "启用" : "停用"}
        </el-tag>
      )
    },
    {
      label: "操作",
      fixed: "right",
      width: 180,
      slot: "operation"
    }
  ];

  async function onSearch() {}

  /** 新增 */
  const handleAdd = line => {
    openAddFormDialog<FormProps>(
      editForm,
      () => ({
        formInline: {
          id: 0,
          lineId: line.id,
          lineCode: line.lineCode,
          status: 1
        }
      }),
      (item: FormProps) => {
        console.log("add", item.formInline);
        return true;
      },
      () => {
        onSearch();
      }
    );
  };

  /** 编辑 */
  const handleEdit = (row: FormItemProps) => {
    openEditFormDialog<FormProps>(
      editForm,
      () => ({
        formInline: row
      }),
      (item: FormProps) => {
        console.log("edit", item.formInline);
        return true;
      },
      () => {
        onSearch();
      }
    );
  };

  /** 删除 */
  const handleDelete = async (row: FormItemProps) => {
    console.log(row);
    await onSearch();
  };

  return {
    loading,
    columns,
    dataList,
    onSearch,
    handleAdd,
    handleEdit,
    handleDelete
  };
}
