import type { FormRules } from "element-plus";
import { reactive } from "vue";

export const formRules = reactive(<FormRules>{
  opCode: [{ required: true, message: "请输入工序编码", trigger: "blur" }],
  opName: [{ required: true, message: "请输入工序名称", trigger: "blur" }]
});
