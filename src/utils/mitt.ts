import type { Emitter } from "mitt";
import mitt from "mitt";

/** 系统内部事件 */
type InternalEvents = {
  openPanel: string;
  tagViewsChange: string;
  tagViewsShowModel: string;
  logoChange: boolean;
  changLayoutRoute: string;
};

/** 自定义事件 */
type CustomEvents = {};

type Events = InternalEvents & CustomEvents;

export const emitter: Emitter<Events> = mitt<Events>();
